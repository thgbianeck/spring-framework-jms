package br.com.bianeck.springframeworkjms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringFrameworkJmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringFrameworkJmsApplication.class, args);
	}

}
